﻿using CamadaDeControle.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CamadaDeControle.Controllers
{
    public class ProdutoController : Controller
    {
        public ActionResult Editar(int id = 0)
        {
            K19Context ctx = new K19Context();
            Produto p = ctx.Produtos.Find(id);

            if (p == null)
            {
                return HttpNotFound();
            }

            return View(p);
        }

        [HttpPost]
        public ActionResult Editar(Produto p)
        {
            K19Context ctx = new K19Context();
            ctx.Entry(p).State = System.Data.EntityState.Modified;

            ctx.SaveChanges();

            TempData["Mensagem"] = "Produto editado com sucesso!";

            return RedirectToAction("Lista");
        }

        public ActionResult Lista(decimal? precoMinimo, decimal? precoMaximo)
        {
            K19Context ctx = new K19Context();
            var produtos = ctx.Produtos.AsEnumerable();

            if (precoMinimo != null && precoMaximo != null)
            {
                produtos = from p in produtos
                           where p.Preco >= precoMinimo & p.Preco <= precoMaximo
                           select p;
            }

            return View(produtos);
        }

        [HttpGet]
        public ActionResult Cadastra()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Cadastra(Produto p)
        {
            K19Context ctx = new K19Context();
            ctx.Produtos.Add(p);
            ctx.SaveChanges();

            TempData["Mensagem"] = "Produto cadastrado com sucesso!";

            return RedirectToAction("Lista");
        }
    }
}
